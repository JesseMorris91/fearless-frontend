import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Nav from './Nav';
import AttendeeList from "./AttendeeList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }


  return (
  <React.Fragment>
    <BrowserRouter>
      <Nav />

     {/* <div className="container"> */}
        <Routes>
          <Route>
            <Route index element={<MainPage />} />
          </Route>
        </Routes>

        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
        </Routes>

        <Routes>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
        </Routes>

        <Routes>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
        </Routes>

        <Routes>
          <Route path="attendees" element={<AttendeeList attendees={props.attendees} />} />
        </Routes>

        <Routes>
          <Route path="presentations">
            <Route path="new" element={<PresentationForm />} />
          </Route>
        </Routes>

     {/* </div>*/}
    </BrowserRouter>
  </React.Fragment>
  );
}

export default App; // export allows us to have access to this in another file

{/*
<Route path="conferences/new" element={<ConferenceForm />} />
        <Route path="attendees/new" element={<AttendConferenceForm />} />
        <Route path="locations/new" element={<LocationForm />} />
        <Route path="attendees" element={<AttendeeList attendees={props.attendees} />} />
*/}

// function App(props) {
//   return (
//     <div>
//       <table>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           for (let attendee of props.attendees) {
//             <tr>
//               <td>{ attendee.name }</td>
//               <td>{ attendee.conference }</td>
//             </tr>
//           }
//         </tbody>
//       </table>
//     </div>
//   );



//function JokesList()

//const get Data = asyna () => {
 // const resp = await fetch(http)
 // const data = await resp.json
//}


//setJokes(data)


//useEffect( () => {}
// <button onClick{() => {
// console.log("Delete joke", joke.id)
// const resp = await fetch ('http://localhost:8000/api/jokes/${joke.id}', {method:"DELETE"})
// }}



//   if (props.attendees === undefined) {
//     return null;
//   }

//   return (
//     <div>
//       Number of attendees: {props.attendees.length}
//     </div>
//   );

// <td>
// <button onClick={handleDelete(joke.id)}}>Delete</button>
// <td>
