import React, { Component } from 'react';


class LocationForm extends React.Component {
    constructor(props) {  // initializing local state by assigning an object to this.state
        super(props) // calling constructor on parent class
        this.state = {states: []}; //adding default state with states key and empty list
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleRoomChange = this.handleRoomChange.bind(this);
        this.handleCityChange = this.handleCityChange.bind(this);
        this.handleStateChange = this.handleStateChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        // to use a method in the class as an event handler, you have to bind
        // if you have an object and refer to one of its methods like a property
        //javascript forget what object it came from
    }


    async handleSubmit(event) {
        event.preventDefault();
        // copies all of the properties and values from this.state into a
        // a new object
        const data = {...this.state};
       // data.room_count = data.roomCount;
        delete data.roomCount;
        delete data.states;
        console.log(data);

        // grab'd the POST code from new-location.js
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data), // added stringify to json so we can convert JS values
            headers: {                 // to a JSON string
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          //  formTag.reset(); - removed since no formTag
            const newLocation = await response.json();
            console.log(newLocation);

            // clears out form after submitting with fields to clear
            const cleared = {
                name: '',
                room_count: '',
                city: '',
                state: '',
              };
              this.setState(cleared);


        }
    }




// event paramter is the event that occurred
// target property is the html tag that caused the event
// event.target is input for locations name, value property is the value in the input of form
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleRoomChange(event) {
        const value = event.target.value;
        this.setState({room_count: value})
    }

    handleCityChange(event) {
        const value = event.target.value;
        this.setState({city: value})
    }

    handleStateChange(event) {
        const value = event.target.value;
        this.setState({state: value})
    }


    // when component first loads there is no array of US states in the
    // components state - timing issue
    async componentDidMount() {

        const url = 'http://localhost:8000/api/states/';

            const response = await fetch(url);

            if (response.ok) {
              const data = await response.json();
              this.setState({states: data.states});

            }
          }


    render() {
        return (


     <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={this.handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                {/* onChange event handler updates data when name is entered into form */}
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleRoomChange} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleCityChange} placeholder="City" required type="text" name="city" id="city" className="form-control" />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleStateChange} required name="state" id="state" className="form-select">
                  <option value="">Choose a state</option>
                  {/*  (state is react lingo / states is our object) for loop to retrieve states list */}
                  {this.state.states.map(state => {
                    return (
                        /* adding a key when we use map, we would usually use ID, but not state abbreviations are the same */
                        <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                        </option>
                        /* injecting the states names into our option value in the form */
                    );
                  })}
                </select>
              </div>
              <button onChange={this.handleSubmit} className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}








/*  export allows us to acces this in another file */

export default LocationForm;
