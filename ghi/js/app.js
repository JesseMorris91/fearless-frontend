function createCard(title, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col-sm-6 col-sm-3 mb-4">
        <div class="shadow-lg p-3 mb-4 bg-body rounded">
            <div class="card">
              <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                    <h5 class="card-title">${title}</h5>
                     <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                      <p class="card-text">${description}</p>
                       <blockquote class="blockquote mb-0">
                      <p>Conference dates</p>
                    <footer class="blockquote-footer">${starts} - ${ends}</footer>
                </blockquote>
            </div>
        </div>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {



    const url = 'http://localhost:8000/api/conferences/';



    try {
        const response = await fetch(url);

        if (!response.ok) {

            return Promise.reject('response not ok')
          // Figure out what to do when the response is bad



        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                  const title = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;

                  const location = details.conference.location['name'];
                  const confDate = new Date(details.conference.starts);
                  const starts = `${confDate.getMonth()}/${confDate.getDate()}/${confDate.getFullYear()}`;
                  const ends = `${confDate.getMonth()}/${confDate.getDate()}/${confDate.getFullYear()}`;
                  const html = createCard(title, description, pictureUrl, starts, ends, location);
                  const columns = document.querySelector('.row')
                  columns.innerHTML += html;

                }
              }



        }
      } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
      }



});
